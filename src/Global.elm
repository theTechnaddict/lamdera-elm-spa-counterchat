module Global exposing
    ( Model
    , Msg(..)
    , init
    , navigate
    , subscriptions
    , update
    , view
    )

import Browser exposing (Document)
import Browser.Navigation as Nav exposing (Key)
import Components
import Generated.Route as Route exposing (Route)
import Set exposing (Set)
import Task
import Url exposing (Url)



-- INIT


type alias Model =
    { url : Url
    , key : Key
    , clientId : String
    , counter : Int
    }


init : Url -> Key -> ( Model, Cmd Msg )
init url key =
    ( { url = url
      , key = key
      , clientId = ""
      , counter = 0
      }
    , Cmd.none
    )



-- UPDATE


type Msg
    = Navigate Route
    | GotNewCounterValue Int String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Navigate route ->
            ( model
            , Nav.pushUrl model.key (Route.toHref route)
            )

        GotNewCounterValue newValue clientId ->
            ( { model | clientId = clientId, counter = newValue }
            , Cmd.none
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


view :
    { page : Document msg
    , global : Model
    , toMsg : Msg -> msg
    }
    -> Document msg
view { page, global, toMsg } =
    Components.layout
        { page = page
        }



-- COMMANDS


send : msg -> Cmd msg
send =
    Task.succeed >> Task.perform identity


navigate : Route -> Cmd Msg
navigate route =
    send (Navigate route)
