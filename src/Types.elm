module Types exposing (..)

import Browser exposing (UrlRequest)
import Browser.Navigation exposing (Key)
import Generated.Pages as Pages
import Generated.Route as Route exposing (Route)
import Global
import Lamdera exposing (ClientId)
import Set exposing (Set)
import Url exposing (Url)


type alias FrontendModel =
    { key : Key
    , url : Url
    , global : Global.Model
    , page : Pages.Model
    }


type alias BackendModel =
    { clients : Set ClientId
    , counter : Int
    }


type FrontendMsg
    = LinkClicked UrlRequest
    | UrlChanged Url
    | Global Global.Msg
    | Page Pages.Msg


type ToBackend
    = ClientJoin


type BackendMsg
    = NoOpBackendMsg


type ToFrontend
    = CounterNewValue Int String
